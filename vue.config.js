const path = require('path')

const isDev = process.env.NODE_ENV !== 'production'

module.exports = {
  lintOnSave: isDev,
  runtimeCompiler: true,
  productionSourceMap: false,
  css: {
    sourceMap: true,
    loaderOptions: {
      scss: {
        additionalData: `
        @import "~@/assets/scss/__variables.scss";
        `,
      },
    },
  },
  configureWebpack: {
    output: {
      filename: isDev ? '[name].[hash:8].js' : '[name].[contenthash:8].js',
      chunkFilename: isDev ? '[name].js' : '[name].[contenthash:8].js',
    },
  },
  devServer: {
    disableHostCheck: true,
    progress: false,
    compress: true,
    watchOptions: {
      ignored: ['node_modules'],
      aggregateTimeout: 1000,
      poll: 1500,
    },
    proxy: {
      '^/api': {
        target: 'http://localhost:7000/',
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/api': '/api',
        },
      },
    },
  },
}
