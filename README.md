# webrtc.client

### Версия Node.js

```sh
14.17.0
```

### Установка

Установка зависимостей

```sh
yarn install
```

### Разработка

Запуск сервера в режиме разработки:

```sh
yarn serve
```

Генерация API из локального сервера NodeJs

```sh
yarn generate:api
```

### Необходимо сделать (TODO:)

1. Доработка и перевод на Vue 3 composition api UI компоненты

#### [Рекомендации](https://ru.vuejs.org/v2/style-guide/index.html)

- Работать только со сборщиком Yarn
- Придерживаться стиля именования компонент
