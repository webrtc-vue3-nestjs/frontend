import { OpenAPI } from '@/api'
import Cookies from 'js-cookie'

export const getToken = (): string | undefined => {
  return Cookies.get('access_token')
}

export const setOpenApiToken = (token?: string): void => {
  token ? (OpenAPI.TOKEN = token) : (OpenAPI.TOKEN = getToken() || '')
}

export const setToken = (token: string): void => {
  Cookies.set('access_token', token, { expires: 1 / 24 })
  setOpenApiToken(token)
}

export const removeToken = (): void => {
  Cookies.remove('access_token')
  OpenAPI.TOKEN = ''
}
