import { NavigationGuardNext, RouteLocationNormalized } from 'vue-router'
import { getToken } from './token'

export const isAuth = (): boolean => {
  return !!getToken()
}

export const authMiddleware = (
  to: RouteLocationNormalized,
  from: RouteLocationNormalized,
  next: NavigationGuardNext
): void => {
  if (!isAuth()) next({ name: 'Login' })
  next()
}

export const loginMiddleware = (
  to: RouteLocationNormalized,
  from: RouteLocationNormalized,
  next: NavigationGuardNext
): void => {
  if (isAuth()) next({ name: 'Chat' })
  next()
}
