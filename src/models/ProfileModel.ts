import { UserDto } from '@/api'

export class ProfileModel implements UserDto {
  constructor(form?: UserDto) {
    this.name = form?.name || ''
    this.login = form?.login || ''
    this.email = form?.email || ''
    this.password = form?.password || ''
  }
  name: string
  login: string
  email: string
  password: string
}
