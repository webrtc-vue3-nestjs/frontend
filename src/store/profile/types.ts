import { CommitOptions, DispatchOptions, Store as VuexStore } from 'vuex'
import { IRootState } from '..'
import { ProfileActions } from './actions'
import { ProfileGetters } from './getters'
import { ProfileMutations } from './mutations'

export type ProfileStore = Omit<VuexStore<IRootState>, 'getters' | 'commit' | 'dispatch'> & {
  commit<K extends keyof ProfileMutations, P extends Parameters<ProfileMutations[K]>[1]>(
    key: K,
    payload: P,
    options?: CommitOptions
  ): ReturnType<ProfileMutations[K]>
} & {
  dispatch<K extends keyof ProfileActions>(
    key: K,
    payload?: Parameters<ProfileActions[K]>[1],
    options?: DispatchOptions
  ): ReturnType<ProfileActions[K]>
} & {
  getters: {
    [K in keyof ProfileGetters]: ReturnType<ProfileGetters[K]>
  }
}
