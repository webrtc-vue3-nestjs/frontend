import { UserDto } from '@/api'
import { MutationTree } from 'vuex'
import { ProfileState } from './state'

export enum ProfileMutationType {
  ChangeProfileUser = 'CHANGE_PROFILE_USER',
}

export type ProfileMutations = {
  [ProfileMutationType.ChangeProfileUser](state: ProfileState, user: UserDto): void
}

export const mutations: MutationTree<ProfileState> & ProfileMutations = {
  [ProfileMutationType.ChangeProfileUser](state, user) {
    state.user = user
  },
}
