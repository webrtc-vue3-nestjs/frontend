import { ActionContext, ActionTree } from 'vuex'
import { IRootState } from '..'
import { ProfileMutations } from './mutations'
import { ProfileState } from './state'

export enum ProfileActionsTypes {}

type ProfileActionArguments = Omit<ActionContext<ProfileState, IRootState>, 'commit'> & {
  commit<K extends keyof ProfileMutations>(
    key: K,
    payload: Parameters<ProfileMutations[K]>[1]
  ): ReturnType<ProfileMutations[K]>
}

export type ProfileActions = {}

export const actions: ActionTree<ProfileState, IRootState> & ProfileActions = {}
