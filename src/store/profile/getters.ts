import { GetterTree } from 'vuex'
import { IRootState } from '..'
import { ProfileState } from './state'

export type ProfileGetters = {}

export const getters: GetterTree<ProfileState, IRootState> & ProfileGetters = {}
