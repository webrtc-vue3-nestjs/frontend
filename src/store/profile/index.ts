import { Module, useStore as useVuexStore } from 'vuex'
import { IRootState } from '..'
import { ProfileState } from './state'
import { state } from './state'
import { getters } from './getters'

import { mutations } from './mutations'

import { actions } from './actions'
import { ProfileStore } from './types'

const auth: Module<ProfileState, IRootState> = {
  state,
  getters,
  mutations,
  actions,
}

export function useProfileStore(): ProfileStore {
  return useVuexStore() as ProfileStore
}

export default auth
