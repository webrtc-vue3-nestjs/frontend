import { ProfileModel } from '@/models/ProfileModel'
import { UserDto } from 'generated/api'

export type ProfileState = {
  user: UserDto
}

export const state: ProfileState = {
  user: new ProfileModel(),
}
