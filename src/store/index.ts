import { createStore, Module, ModuleTree } from 'vuex'
import authModule from './auth'
import profileModule from './profile'
export interface IRootState {
  version: string
}
const modules: ModuleTree<IRootState> = {
  authModule,
  profileModule,
}

const root: Module<IRootState, IRootState> = {
  state: {
    version: '1.0.0',
  },
  modules,
}

export default createStore(root)
