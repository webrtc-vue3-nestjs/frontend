import { AuthService, LoginRequestDto, LoginResponseDto, UserDto } from '@/api'
import { removeToken, setToken } from '@/tools/token'
import router from '@/router'
import { ActionContext, ActionTree } from 'vuex'
import { IRootState } from '..'
import { AuthMutations, AuthMutationType } from './mutations'
import { AuthState } from './state'
import { useProfileStore } from '../profile'
import { ProfileMutationType } from '../profile/mutations'

export enum AuthActionsTypes {
  login = 'LOGIN',
  register = 'REGISTER',
  logout = 'LOGOUT',
}

type AuthActionArguments = Omit<ActionContext<AuthState, IRootState>, 'commit'> & {
  commit<K extends keyof AuthMutations>(key: K, payload: Parameters<AuthMutations[K]>[1]): ReturnType<AuthMutations[K]>
}

export type AuthActions = {
  [AuthActionsTypes.login](context: AuthActionArguments, form: LoginRequestDto): Promise<UserDto>
  [AuthActionsTypes.register](context: AuthActionArguments, form: UserDto): Promise<UserDto>
  [AuthActionsTypes.logout](context: AuthActionArguments): void
}

export const actions: ActionTree<AuthState, IRootState> & AuthActions = {
  async [AuthActionsTypes.login](_, form: LoginRequestDto) {
    const response: LoginResponseDto = await AuthService.authControllerLogin(form)
    setToken(response.token)
    return response.user
  },

  async [AuthActionsTypes.register](_, form: UserDto) {
    const response: LoginResponseDto = await AuthService.authControllerRegister(form)
    setToken(response.token)
    return response.user
  },

  [AuthActionsTypes.logout]() {
    removeToken()
    router.replace({ name: 'Login' })
  },
}
