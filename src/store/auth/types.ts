import { CommitOptions, DispatchOptions, Store as VuexStore } from 'vuex'
import { IRootState } from '..'
import { AuthActions } from './actions'
import { AuthGetters } from './getters'
import { AuthMutations } from './mutations'

export type AuthStore = Omit<VuexStore<IRootState>, 'getters' | 'commit' | 'dispatch'> & {
  commit<K extends keyof AuthMutations, P extends Parameters<AuthMutations[K]>[1]>(
    key: K,
    payload: P,
    options?: CommitOptions
  ): ReturnType<AuthMutations[K]>
} & {
  dispatch<K extends keyof AuthActions>(
    key: K,
    payload?: Parameters<AuthActions[K]>[1],
    options?: DispatchOptions
  ): ReturnType<AuthActions[K]>
} & {
  getters: {
    [K in keyof AuthGetters]: ReturnType<AuthGetters[K]>
  }
}
