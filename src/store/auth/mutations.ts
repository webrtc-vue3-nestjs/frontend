import { MutationTree } from 'vuex'
import { AuthState } from './state'

export enum AuthMutationType {
  ChangeAuthStatus = 'CHANGE_AUTH_STATUS',
  ChangeToken = 'CHANGE_TOKEN',
}

export type AuthMutations = {
  [AuthMutationType.ChangeAuthStatus](state: AuthState, isAuth: boolean): void
  [AuthMutationType.ChangeToken](state: AuthState, token: string): void
}

export const mutations: MutationTree<AuthState> & AuthMutations = {
  [AuthMutationType.ChangeAuthStatus](state, isAuth) {
    state.isAuth = isAuth
  },

  [AuthMutationType.ChangeToken](state, token) {
    state.token = token
  },
}
