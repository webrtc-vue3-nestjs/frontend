import { Module, useStore as useVuexStore } from 'vuex'
import { IRootState } from '..'
import { AuthState } from './state'
import { state } from './state'
import { getters } from './getters'

import { mutations } from './mutations'

import { actions } from './actions'
import { AuthStore } from './types'

const auth: Module<AuthState, IRootState> = {
  state,
  getters,
  mutations,
  actions,
}

export function useAuthStore(): AuthStore {
  return useVuexStore() as AuthStore
}

export default auth
