import { GetterTree } from 'vuex'
import { IRootState } from '..'
import { AuthState } from './state'

export type AuthGetters = {
  token(state: AuthState): string
}

export const getters: GetterTree<AuthState, IRootState> & AuthGetters = {
  token(state) {
    return state.token
  },
}
