export type AuthState = {
  token: string
  isAuth: boolean
}

export const state: AuthState = {
  token: '',
  isAuth: false,
}
