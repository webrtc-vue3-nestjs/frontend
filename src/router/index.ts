import { authMiddleware, loginMiddleware } from '@/tools/security'
import {
  createRouter,
  createWebHistory,
  NavigationGuardNext,
  RouteLocationNormalized,
  RouteRecordRaw,
} from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Chat',
    beforeEnter(to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext) {
      authMiddleware(to, from, next)
    },
    component: () => import('../views/Chat.vue'),
  },
  {
    path: '/login',
    name: 'Login',
    beforeEnter(to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext) {
      loginMiddleware(to, from, next)
    },
    component: () => import('../views/Login.vue'),
  },
  {
    path: '/register',
    name: 'Register',
    beforeEnter(to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext) {
      loginMiddleware(to, from, next)
    },
    component: () => import('../views/Register.vue'),
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
